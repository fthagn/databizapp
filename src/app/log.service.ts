import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LogService {

  logs: string[] = [];

  constructor() { }

  add(msg) {
    this.logs.push(msg);
  }
}
