import { Component, OnInit } from '@angular/core';

import { Chart } from 'chart.js';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { interval } from 'rxjs';
// import { Firebase } from '@ionic-native/firebase/ngx';
import { Platform, ToastController } from '@ionic/angular';
import { LogService } from '../log.service';

@Component({
  selector:  'app-home',
  templateUrl:  'home.page.html',
  styleUrls:  ['home.page.scss'],
})
export class HomePage implements OnInit {
  LineChart = [];
  // BarChart = [];
  // PieChart = [];

  authTokens = [
   '31bbd65412b74385d46cf9b062d72ccce6961ac3',
   '9ba5a3158e6df6ba51fbf20eb544d147a8d10e38',
   '8b8b6ae590cead728051f96adad76a01d151cc4a',
   'e3ca6a1ebd389fda85f148eb4d760452219a2c60',
   'dae9c0764be09e34f423781748434ec0e9484abb',
   '5596eff34b21031c00660c033ef8ec8fbc61d7f6',
   '594af9c0d64683d73a67bed6164eba9b051a6d1a',
   '2c49bd10ce6fafc23228d18b951e076992a863e6',
   '35d34eea8590c8256efd88cb06840657af01c5dc',
   'bc18a37b3cd9c81b4cf5391652b5a32b39a08ca5',
   'cc8eb15bbbcb5dc5a6de86b9e08242b0f90ae0f7',
   '2aa99ea8b42e7d2fec06d747073314efff877d0e',
   '683e775e730bbf9c67af32fee60c3d3738fc1f99',
   '37b86970eba27bee586a098191373d855d6136f9',
   '68f4ac164cb76980bbd92274946179a7c9469ac5',
   'b1ba3a31558fda1cdaaba39db9b988ff25089b00',
   'c88fd372bd2f93bd9e0006120146502fad6423ca',
   '6a27ab2c7c03c585b6637e8e7f50fae078266915',
   '8314ca3805c88e6f23c53b3fce01bfc03909dfed',
   '3b4ac0750ec2ffaae7e88b683d867b7a82a4f775',
   'e4443306c649f6cb4fb468ded5ac792fbe87c69c',
   '10ed5bd4b72a7a41e5ee9a088936f1739e799bf2',
   '7abfff5c9802cfe637dace52fb9ab34d50f375ce',
   'ad1bc931d1fabf2380672c1e5a17f45e1edd6ea9',
   '598b6364ae36c3b42ed52dfc8cb9f4968c17955d',
   'c6a1a54272aef231f69b9e967a545b19b9cad020',
   'cfc8a492f0c711260e3991ef1fba204cb012fc58',
   '29214c74361dba578309ee2f1b7b45cc6dfd97c9',
   'af10cce82bef97255b1d9a56c6b3ca58faa1a342',
   'f6bc08bc02ac775b3a7b30f535fc17ae8f4c6836',
   '5c75d47604f644aa8871bdebd65c16f542af04f1',
   '9f31c40a657bc1af861408b5446690128f1d785e',
   '8573f66f8a4f14e8c4dba2c691ad8fcfc981b0e0',
   'b9e3fa0f2f92f301fb9e91532dab09459395fad5',
   'ed8d52bb2f1a36b1a0389c78bd37757c87dfda00'
  ];

  userId = 1;
  baseUrl = 'https://databiz.herokuapp.com/energy_data/';
  getUrl = this.baseUrl + 'get';
  notifyUrl = this.baseUrl + 'notify';
  data: number[] = [];
  labels = [];
  currentDate = new Date('2014-01-02 00:00:00');
  fromDate = (new Date()).setDate(this.currentDate.getDate() - 1);
  sub: any;
  syncInterval = 1000; // milliseconds

  constructor(private http: HttpClient,
              // private firebase: Firebase,
              private logs: LogService,
              private platform: Platform,
              private toastController: ToastController) {}

  httpOptions() {
    return {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: 'Token ' + this.authTokens[this.userId]
      })
    };
  }

  getNotifications() {
    this.http.get(this.notifyUrl, this.httpOptions()).subscribe((res: string) => {
      const response = JSON.parse(res);
      console.log(response);
      if (response !== '') {
        this.logs.add(response);
        this.presentToast(response);

      }
    });
  }

  getData() {
    this.http.get(this.getUrl, this.httpOptions()).subscribe((res: string) => {
      const response = JSON.parse(res);
      console.log(response);
      this.labels = [];
      this.data = [];
      for (const datum of response) {
        this.labels.push(datum.timestamp);
        this.data.push(datum.value);
      }
    });
    this.LineChart = new Chart('lineChart', {
      type:  'line',
      data:  {
        labels:  this.labels,
        datasets:  [{
          label:  'Messwerte in Watt',
          data:  this.data,
          fill: false,
          lineTension: 0.2,
          borderColor: 'red',
          borderWidth:  1
        }]
      },
      options:  {
        animation: false,
        title: {
          text: 'Messwerte in Watt',
          display: false
        },
        scales:  {
          yAxes:  [{
            ticks:  {
              beginAtZero: true
            }
          }]
        }
      }
    });
  }

  private async presentToast(message) {
    const toast = await this.toastController.create({
      message,
      duration: 3000
    });
    toast.present();
  }

  // async setToken() {
  //   (await this.sendDeviceToken()).subscribe();
  // }
  //
  // async sendDeviceToken() {
  //   const body = JSON.stringify({
  //     token: await this.getDeviceToken()
  //   });
  //
  //   return this.http.post(this.deviceUrl, body, this.httpOptions());
  // }
  //
  // async getDeviceToken() {
  //   let token;
  //
  //   if (this.platform.is('android')) {
  //     token = await this.firebase.getToken();
  //   }
  //
  //   if (this.platform.is('ios')) {
  //     token = await this.firebase.getToken();
  //     await this.firebase.grantPermission();
  //   }
  //
  //   return token;
  // }

  ngOnInit() {
    // if (this.platform.is('ios')) {
    //   this.firebase.grantPermission();
    // }
    // this.firebase.onNotificationOpen().subscribe((msg) => {
    //   // todo process messages in chats (requires testing to see format)
    //   if (this.platform.is('ios')) {
    //     this.presentToast(msg.aps.alert);
    //   } else {
    //     this.presentToast(msg.body);
    //   }
    // });

    this.getData();

    this.sub = interval(this.syncInterval).subscribe(() => {
      this.getData();
      this.getNotifications();
    });

      // Line chart:

 // Bar chart:
 // this.BarChart = new Chart('barChart', {
 //   type:  'bar',
 // data:  {
 //  labels:  ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
 //  datasets:  [{
 //      label:  '# of Votes',
 //      data:  [9, 7 , 3, 5, 2, 10],
 //      backgroundColor:  [
 //          'rgba(255, 99, 132, 0.2)',
 //          'rgba(54, 162, 235, 0.2)',
 //          'rgba(255, 206, 86, 0.2)',
 //          'rgba(75, 192, 192, 0.2)',
 //          'rgba(153, 102, 255, 0.2)',
 //          'rgba(255, 159, 64, 0.2)'
 //      ],
 //      borderColor:  [
 //          'rgba(255,99,132,1)',
 //          'rgba(54, 162, 235, 1)',
 //          'rgba(255, 206, 86, 1)',
 //          'rgba(75, 192, 192, 1)',
 //          'rgba(153, 102, 255, 1)',
 //          'rgba(255, 159, 64, 1)'
 //      ],
 //      borderWidth:  1
 //  }]
 // },
 // options:  {
 //  title: {
 //      text: 'Bar Chart',
 //      display: true
 //  },
 //  scales:  {
 //      yAxes:  [{
 //          ticks:  {
 //              beginAtZero: true
 //          }
 //      }]
 //  }
 // }
 // });
 //
 // // pie chart:
 // this.PieChart = new Chart('pieChart', {
 //   type:  'pie',
 // data:  {
 //  labels:  ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
 //  datasets:  [{
 //      label:  '# of Votes',
 //      data:  [9, 7 , 3, 5, 2, 10],
 //      backgroundColor:  [
 //          'rgba(255, 99, 132, 0.2)',
 //          'rgba(54, 162, 235, 0.2)',
 //          'rgba(255, 206, 86, 0.2)',
 //          'rgba(75, 192, 192, 0.2)',
 //          'rgba(153, 102, 255, 0.2)',
 //          'rgba(255, 159, 64, 0.2)'
 //      ],
 //      borderColor:  [
 //          'rgba(255,99,132,1)',
 //          'rgba(54, 162, 235, 1)',
 //          'rgba(255, 206, 86, 1)',
 //          'rgba(75, 192, 192, 1)',
 //          'rgba(153, 102, 255, 1)',
 //          'rgba(255, 159, 64, 1)'
 //      ],
 //      borderWidth:  1
 //  }]
 // },
 // options:  {
 //  title: {
 //      text: 'Bar Chart',
 //      display: true
 //  },
 //  scales:  {
 //      yAxes:  [{
 //          ticks:  {
 //              beginAtZero: true
 //          }
 //      }]
 //  }
 // }
 // });


   }
}
